// ignore_for_file: prefer_const_constructors

import 'package:carousel_slider/carousel_slider.dart';
import 'package:enter_tain/backend/film_controller.dart';
import 'package:enter_tain/mainTabs/filmTabs/views/movie_view.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ActionMovieTab extends StatefulWidget {
  const ActionMovieTab({super.key});

  @override
  State<ActionMovieTab> createState() => _ActionMovieTabState();
}

class _ActionMovieTabState extends State<ActionMovieTab> {
  double? height;
  double? width;
  @override
  Widget build(BuildContext context) {
    height = MediaQuery.of(context).size.height;
    width = MediaQuery.of(context).size.width;
    return GetBuilder<FilmController>(
      init: FilmController(),
      builder: (controller) {
        return controller.isLoading
            ? Center(
                child: CircularProgressIndicator(),
              )
            : CarouselSlider.builder(
                itemCount: controller.filmCatagory!.results.length,
                itemBuilder: (context, index, i) {
                  return InkWell(
                    onTap: () {
                      controller.castReport(
                          controller.filmCatagory!.results[index].id);
                      // debugPrint(
                      //     "----------------${controller.filmdetails!.credits!.cast[index].toString()}");
                      if (!mounted) return;
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: ((context) => MovieViewPage(
                                    index: index,
                                  ))));
                    },
                    child: Container(
                      width: width! * .45,
                      margin: EdgeInsets.all(5.0),
                      child: Column(
                        children: [
                          ClipRRect(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(15.0)),
                              child: Image.network(
                                "https://image.tmdb.org/t/p/original${controller.filmCatagory!.results[index].posterPath.toString()}",
                                fit: BoxFit.cover,
                                height: height! * .28,
                              )),
                          SizedBox(
                            height: 15,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            // Sections Under the the Carousal Slider
                            children: [
                              Container(
                                padding: EdgeInsets.all(5),
                                decoration: BoxDecoration(
                                    color: Color.fromARGB(83, 158, 158, 158),
                                    borderRadius: BorderRadius.circular(10)),
                                child: Text(
                                  controller.filmCatagory!.results[index].adult!
                                      ? "18+"
                                      : "5+",
                                  style: TextStyle(color: Colors.white),
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.all(5),
                                decoration: BoxDecoration(
                                    color: Color.fromARGB(83, 158, 158, 158),
                                    borderRadius: BorderRadius.circular(10)),
                                child: Text(
                                  "Action",
                                  style: TextStyle(color: Colors.white),
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.all(5),
                                decoration: BoxDecoration(
                                    color: Color.fromARGB(83, 158, 158, 158),
                                    borderRadius: BorderRadius.circular(10)),
                                child: Row(
                                  children: [
                                    Icon(
                                      Icons.star,
                                      size: 18,
                                      color: Colors.yellow,
                                    ),
                                    Text(
                                      controller.filmCatagory!.results[index]
                                          .voteAverage
                                          .toString(),
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 15),
                                    )
                                  ],
                                ),
                              )
                            ],
                          ),
                          Container(
                            padding: EdgeInsets.only(top: 5),
                            // alignment: Alignment.topLeft,
                            child: Text(
                              controller.filmCatagory!.results[index].title
                                  .toString(),
                              style:
                                  TextStyle(color: Colors.white, fontSize: 22),
                            ),
                          )
                        ],
                      ),
                    ),
                  );
                },
                options: CarouselOptions(
                  viewportFraction: .60,
                  autoPlayAnimationDuration:
                      Duration(seconds: 1, milliseconds: 50),
                  autoPlay: true,
                  aspectRatio: .25,
                  enlargeCenterPage: true,
                  enlargeStrategy: CenterPageEnlargeStrategy.zoom,
                ),
              );
      },
    );
  }
}
