// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:expandable_text/expandable_text.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../backend/film_controller.dart';

class MovieViewPage extends StatefulWidget {
  final int index;
  MovieViewPage({super.key, required this.index});

  @override
  State<MovieViewPage> createState() => _MovieViewPageState();
}

class _MovieViewPageState extends State<MovieViewPage> {
  double? height;
  double? width;

  @override
  Widget build(BuildContext context) {
    height = MediaQuery.of(context).size.height;
    width = MediaQuery.of(context).size.width;
    return GetBuilder<FilmController>(
        init: FilmController(),
        builder: (controller) {
          return controller.isLoading
              ? Center(
                  child: CircularProgressIndicator(),
                )
              : SingleChildScrollView(
                  child: Container(
                    height: height,
                    width: width,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                          image: NetworkImage(
                              "https://image.tmdb.org/t/p/original${controller.filmdetails!.posterPath.toString()}"),
                          fit: BoxFit.cover),
                    ),
                    child: Scaffold(
                      appBar: AppBar(
                        toolbarHeight: 20,
                        automaticallyImplyLeading: false,
                        leading: InkWell(
                            onTap: () {
                              Get.back();
                            },
                            child: const Icon(Icons.arrow_back_rounded)),
                        backgroundColor: Colors.transparent,
                        elevation: 0,
                      ),
                      backgroundColor: Colors.transparent,
                      body: Container(
                        height: height,
                        width: width,
                        padding: EdgeInsets.all(20),
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                            end: Alignment.center,
                            begin: Alignment.topCenter,
                            colors: [
                              Colors.transparent,
                              Color.fromARGB(249, 0, 0, 0),
                              // Colors.black,
                            ],
                          ),
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Expanded(
                              child: Align(
                                child: CircleAvatar(
                                  backgroundColor:
                                      Color.fromARGB(139, 255, 82, 82),
                                  child: Icon(
                                    Icons.play_arrow,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                            ),
                            Align(
                              alignment: Alignment.bottomRight,
                              child: Text(
                                "${(int.parse(controller.filmdetails!.runtime.toString()) / 60).floor()}h${(int.parse(controller.filmdetails!.runtime.toString()) % 60).floor()}min",
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              // Sections Under view
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(right: 8.0),
                                  child: Container(
                                    padding: EdgeInsets.all(5),
                                    decoration: BoxDecoration(
                                        color:
                                            Color.fromARGB(83, 158, 158, 158),
                                        borderRadius:
                                            BorderRadius.circular(10)),
                                    child: Text(
                                      controller.films!.results[widget.index]
                                              .adult!
                                          ? "18+"
                                          : "5+",
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(right: 8.0),
                                  child: Container(
                                    padding: EdgeInsets.all(5),
                                    decoration: BoxDecoration(
                                        color:
                                            Color.fromARGB(83, 158, 158, 158),
                                        borderRadius:
                                            BorderRadius.circular(10)),
                                    child: Text(
                                      controller.filmdetails!.genres[0].name
                                                  .toString() ==
                                              "Science Fiction"
                                          ? "Sci-Fi"
                                          : controller
                                              .filmdetails!.genres[0].name
                                              .toString(),
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  ),
                                ),
                                Container(
                                  padding: EdgeInsets.all(5),
                                  decoration: BoxDecoration(
                                      color: Color.fromARGB(83, 158, 158, 158),
                                      borderRadius: BorderRadius.circular(10)),
                                  child: Row(
                                    children: [
                                      Icon(
                                        Icons.star,
                                        size: 18,
                                        color: Colors.yellow,
                                      ),
                                      Text(
                                        controller.films!.results[widget.index]
                                            .voteAverage
                                            .toString(),
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 15),
                                      )
                                    ],
                                  ),
                                ),
                                Expanded(
                                  child: SizedBox(
                                      child: Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                      IconButton(
                                        icon: Icon(
                                          Icons.add_box_outlined,
                                          color: Colors.white,
                                        ),
                                        onPressed: () {},
                                      ),
                                      IconButton(
                                        icon: Icon(
                                          Icons.send,
                                          color: Colors.white,
                                        ),
                                        onPressed: () {},
                                      ),
                                    ],
                                  )),
                                )
                              ],
                            ),
                            ListTile(
                              title: Text(
                                controller.filmdetails!.title.toString(),
                                style: TextStyle(
                                    color: Colors.white, fontSize: 22),
                              ),
                              subtitle: ExpandableText(
                                controller.filmdetails!.overview.toString(),
                                expandText: 'show more',
                                maxLines: 4,
                                linkColor: Colors.blue,
                                linkStyle: TextStyle(color: Colors.white),
                                animation: true,
                                animationDuration: Duration(seconds: 2),
                                collapseOnTextTap: true,
                                style: TextStyle(color: Colors.white),
                                prefixStyle:
                                    TextStyle(fontWeight: FontWeight.bold),
                                mentionStyle: TextStyle(
                                  fontWeight: FontWeight.w600,
                                ),
                                urlStyle: TextStyle(
                                  decoration: TextDecoration.underline,
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 170,
                              child: ListView.builder(
                                  shrinkWrap: true,
                                  itemCount: controller
                                      .filmdetails!.credits!.cast.length,
                                  scrollDirection: Axis.horizontal,
                                  itemBuilder: (BuildContext context, index) {
                                    return Column(
                                      mainAxisSize: MainAxisSize.min,
                                      children: [
                                        Container(
                                          height: 140,
                                          width: 130,
                                          margin: EdgeInsets.only(
                                              left: 9, bottom: 10),
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(20),
                                            image: DecorationImage(
                                                image: NetworkImage(
                                                    "https://image.tmdb.org/t/p/original${controller.filmdetails!.credits!.cast[index].profilePath.toString()}"),
                                                fit: BoxFit.cover),
                                          ),
                                        ),
                                        Text(
                                          controller.filmdetails!.credits!
                                              .cast[index].name
                                              .toString(),
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 15),
                                          softWrap: true,
                                        )
                                      ],
                                    );
                                  }),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                );
        });
  }
}
