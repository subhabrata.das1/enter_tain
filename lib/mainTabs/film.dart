// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:carousel_slider/carousel_slider.dart';
import 'package:enter_tain/cutom_widgets/custom_carousal.dart';
import 'package:enter_tain/mainTabs/filmTabs/action_movies.dart';
import 'package:enter_tain/mainTabs/filmTabs/allmovies.dart';

import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../backend/film_controller.dart';

class FilmPage extends StatefulWidget {
  const FilmPage({super.key});

  @override
  State<FilmPage> createState() => _FilmPageState();
}

class _FilmPageState extends State<FilmPage>
    with SingleTickerProviderStateMixin {
  late TabController _tabController1;
  double? height;
  double? width;
  FilmController? controller1;

  // final List<String> imgList = [
  //   'https://images.unsplash.com/photo-1520342868574-5fa3804e551c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=6ff92caffcdd63681a35134a6770ed3b&auto=format&fit=crop&w=1951&q=80',
  //   'https://images.unsplash.com/photo-1522205408450-add114ad53fe?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=368f45b0888aeb0b7b08e3a1084d3ede&auto=format&fit=crop&w=1950&q=80',
  //   'https://images.unsplash.com/photo-1519125323398-675f0ddb6308?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=94a1e718d89ca60a6337a6008341ca50&auto=format&fit=crop&w=1950&q=80',
  // ];

  @override
  void initState() {
    super.initState();
    _tabController1 = TabController(length: 6, vsync: this, initialIndex: 0);
//Carousal Image List

//Carousal Widets
  }

  onTabBarChange(index) {
    debugPrint("---------------------${index.toString()}");
    List<String> catagory = ["", "28", "878", "35", "10749", "14"];
    if (index == 0) {
      controller1!.filmsReport();
    } else {
      controller1!.movieCatagory(catagory[index]);
    }
  }

//Middle tabbar
  TabBar get tabs => TabBar(
        isScrollable: true,
        labelColor: Colors.white,
        labelStyle: const TextStyle(
          fontSize: 20,
          fontWeight: FontWeight.bold,
        ),
        splashBorderRadius: BorderRadius.circular(5),
        controller: _tabController1,
        unselectedLabelColor: Colors.grey[400],
        dividerColor: Colors.transparent,
        labelPadding: EdgeInsets.symmetric(horizontal: 9),
        onTap: (ind) {
          onTabBarChange(ind);
        },
        indicator: BoxDecoration(
            color: Colors.red, borderRadius: BorderRadius.circular(10.0)),
        tabs: const [
          Tab(
            text: "All",
          ),
          Tab(
            text: "Action",
          ),
          Tab(
            text: "Sci Fi",
          ),
          Tab(
            text: "Comedy",
          ),
          Tab(
            text: "Romance",
          ),
          Tab(
            text: "Fantasy",
          ),
        ],
      );

  @override
  Widget build(BuildContext context) {
    height = MediaQuery.of(context).size.height;
    width = MediaQuery.of(context).size.width;
    return GetBuilder<FilmController>(
        init: FilmController(),
        builder: (controller) {
          controller1 = controller;
          return controller.isLoading
              ? Center(
                  child: CircularProgressIndicator(),
                )
              : Scaffold(
                  backgroundColor: Colors.black45,
                  body: SingleChildScrollView(
                    child: SizedBox(
                      height: height,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          //top Vdo view section
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: ListTile(
                              title: const Padding(
                                padding: EdgeInsets.symmetric(vertical: 10),
                                child: Text(
                                  "Comming Soon",
                                  style: TextStyle(
                                    fontSize: 25,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                              subtitle: Container(
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(15),
                                    color: Colors.white,
                                    image: DecorationImage(
                                        fit: BoxFit.cover,
                                        image: NetworkImage(
                                            "https://image.tmdb.org/t/p/original${controller.films!.results[0].posterPath.toString()}"))),
                                alignment: Alignment.center,
                                height: height! * .25,
                                width: width! - 40,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 8.0),
                                      child: Row(
                                        children: [
                                          Text(
                                            controller.films!.results[0].title
                                                .toString(),
                                            style: TextStyle(
                                              fontSize: 20,
                                              color: Colors.white,
                                            ),
                                          ),
                                          Spacer(),
                                          Icon(
                                            Icons.send,
                                            color: Colors.white,
                                            size: 25,
                                          )
                                        ],
                                      ),
                                    ),
                                    SizedBox(
                                      height: height! * .07,
                                    ),
                                    CircleAvatar(
                                      backgroundColor:
                                          Color.fromARGB(139, 255, 82, 82),
                                      child: Icon(
                                        Icons.play_arrow,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          Container(
                              margin: EdgeInsets.only(left: 25, top: 20),
                              height: height! * .04,
                              child: tabs),
                          Container(
                            padding:
                                EdgeInsets.only(top: 20, left: 25, bottom: 15),
                            alignment: Alignment.topLeft,
                            child: Text(
                              "Trending Now",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: height! * .025,
                              ),
                            ),
                          ),

                          Expanded(
                            child: TabBarView(
                              controller: _tabController1,
                              children: [
                                AllMovieTab(),
                                CustomCarousalBuilder(),
                                CustomCarousalBuilder(),
                                CustomCarousalBuilder(),
                                CustomCarousalBuilder(),
                                CustomCarousalBuilder(),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                );
        });
  }
}
