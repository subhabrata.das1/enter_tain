// ignore_for_file: prefer_const_constructors

import 'package:enter_tain/mainTabs/film.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  const HomePage({
    super.key,
  });

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;
  double? height;
  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 3, vsync: this, initialIndex: 1);
  }

  TabBar get tabs => TabBar(
        isScrollable: true,
        padding: const EdgeInsets.all(10),
        labelColor: Colors.red,
        labelStyle: const TextStyle(fontSize: 22),
        splashBorderRadius: BorderRadius.circular(5),
        controller: _tabController,
        automaticIndicatorColorAdjustment: false,
        unselectedLabelColor: Colors.grey[200],
        indicatorColor: Colors.transparent,
        dividerColor: Colors.transparent,
        tabs: const [
          Tab(
            text: "Series",
          ),
          Tab(
            text: "Film",
          ),
          Tab(
            text: "My List",
          ),
        ],
      );

  @override
  Widget build(BuildContext context) {
    height = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: Colors.transparent,
      appBar: AppBar(
        toolbarHeight: height! * .03,
        backgroundColor: Colors.transparent,
        elevation: 0,
        bottom: PreferredSize(
          preferredSize: tabs.preferredSize,
          child: Material(
            color: Colors.transparent,
            child: tabs,
          ),
        ),
      ),
      body: TabBarView(
        controller: _tabController,
        children: const [
          Center(
            child: Text("It's rainy here"),
          ),
          FilmPage(),
          Center(
            child: Text("It's sunny here"),
          ),
        ],
      ),
    );
  }
}
