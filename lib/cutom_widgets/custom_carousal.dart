import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:get/get.dart';

import '../backend/film_controller.dart';
import '../mainTabs/filmTabs/views/movie_view.dart';

class CustomCarousalBuilder extends StatefulWidget {
  CustomCarousalBuilder({super.key});

  @override
  State<CustomCarousalBuilder> createState() => _CustomCarousalBuilderState();
}

class _CustomCarousalBuilderState extends State<CustomCarousalBuilder> {
  double? height;
  double? width;
  @override
  Widget build(BuildContext context) {
    height = MediaQuery.of(context).size.height;
    width = MediaQuery.of(context).size.width;
    return GetBuilder<FilmController>(
      init: FilmController(),
      builder: (controller) {
        return controller.isLoading && controller.filmCatagory == null
            ? const Center(
                child: CircularProgressIndicator(),
              )
            : CarouselSlider.builder(
                itemCount: controller.filmCatagory!.results.length,
                itemBuilder: (context, index, i) {
                  return InkWell(
                    onTap: () {
                      controller.castReport(
                          controller.filmCatagory!.results[index].id);
                      // debugPrint(
                      //     "----------------${controller.filmdetails!.credits!.cast[index].toString()}");
                      if (!mounted) return;
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: ((context) => MovieViewPage(
                                    index: index,
                                  ))));
                    },
                    child: Container(
                      width: width! * .45,
                      margin: const EdgeInsets.all(5.0),
                      child: Column(
                        children: [
                          ClipRRect(
                              borderRadius:
                                  const BorderRadius.all(Radius.circular(15.0)),
                              child: Image.network(
                                "https://image.tmdb.org/t/p/original${controller.filmCatagory!.results[index].posterPath.toString()}",
                                fit: BoxFit.cover,
                                height: height! * .28,
                              )),
                          const SizedBox(
                            height: 15,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            // Sections Under the the Carousal Slider
                            children: [
                              Container(
                                padding: const EdgeInsets.all(5),
                                decoration: BoxDecoration(
                                    color:
                                        const Color.fromARGB(83, 158, 158, 158),
                                    borderRadius: BorderRadius.circular(10)),
                                child: Text(
                                  controller.filmCatagory!.results[index].adult!
                                      ? "18+"
                                      : "5+",
                                  style: const TextStyle(color: Colors.white),
                                ),
                              ),
                              Container(
                                padding: const EdgeInsets.all(5),
                                decoration: BoxDecoration(
                                    color:
                                        const Color.fromARGB(83, 158, 158, 158),
                                    borderRadius: BorderRadius.circular(10)),
                                child: const Text(
                                  "Action",
                                  style: TextStyle(color: Colors.white),
                                ),
                              ),
                              Container(
                                padding: const EdgeInsets.all(5),
                                decoration: BoxDecoration(
                                    color:
                                        const Color.fromARGB(83, 158, 158, 158),
                                    borderRadius: BorderRadius.circular(10)),
                                child: Row(
                                  children: [
                                    const Icon(
                                      Icons.star,
                                      size: 18,
                                      color: Colors.yellow,
                                    ),
                                    Text(
                                      controller.filmCatagory!.results[index]
                                          .voteAverage
                                          .toString(),
                                      style: const TextStyle(
                                          color: Colors.white, fontSize: 15),
                                    )
                                  ],
                                ),
                              )
                            ],
                          ),
                          Container(
                            padding: const EdgeInsets.only(top: 5),
                            // alignment: Alignment.topLeft,
                            child: Text(
                              controller.filmCatagory!.results[index].title
                                  .toString(),
                              style: const TextStyle(
                                  color: Colors.white, fontSize: 22),
                            ),
                          )
                        ],
                      ),
                    ),
                  );
                },
                options: CarouselOptions(
                  viewportFraction: .60,
                  autoPlayAnimationDuration:
                      const Duration(seconds: 1, milliseconds: 50),
                  autoPlay: true,
                  aspectRatio: .25,
                  enlargeCenterPage: true,
                  enlargeStrategy: CenterPageEnlargeStrategy.zoom,
                ),
              );
      },
    );
  }
}
