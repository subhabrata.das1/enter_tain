import 'dart:convert';

import 'package:enter_tain/backend/models/tmdb/tmdb_movie_catagory.dart';
import 'package:enter_tain/backend/models/tmdb/tmdbcasts_model.dart';
import 'package:enter_tain/backend/models/tmdb/tmdbfilms_model.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;

class FilmController extends GetxController {
  bool isLoading = false;
  Films? films;
  FilmDetails? filmdetails;
  FilmCatagory? filmCatagory;

  @override
  void onInit() {
    super.onInit();
    filmsReport();
  }

  Future filmsReport() async {
    isLoading = true;
    final responce = await http.get(
      Uri.parse(
          'https://api.themoviedb.org/3/discover/movie?api_key=f7945752383a1c55a5df52adda5c3982'),
    );
    final jsondata = jsonDecode(responce.body);
    if (responce.statusCode == 200) {
      isLoading = false;
      films = Films.fromJson(jsondata);
    } else {
      isLoading = false;
    }
    update();
  }

  Future castReport(movieId) async {
    isLoading = true;
    final responce = await http.get(
      Uri.parse(
          'https://api.themoviedb.org/3/movie/$movieId?api_key=f7945752383a1c55a5df52adda5c3982&append_to_response=credits'),
    );
    final jsondata = jsonDecode(responce.body);
    if (responce.statusCode == 200) {
      isLoading = false;
      filmdetails = FilmDetails.fromJson(jsondata);
    } else {
      isLoading = false;
    }
    update();
  }

  Future movieCatagory(String catID) async {
    isLoading = true;
    final responce = await http.get(
      Uri.parse(
          'https://api.themoviedb.org/3/discover/movie?api_key=f7945752383a1c55a5df52adda5c3982&with_genres=$catID'),
    );
    final jsondata = jsonDecode(responce.body);
    if (responce.statusCode == 200) {
      isLoading = false;
      filmCatagory = FilmCatagory.fromJson(jsondata);
    } else {
      isLoading = false;
    }
    update();
  }
}
